const Helpers = require('../helpers.js');

module.exports = {
    name: 'lamar',
    description: "",
	execute(msg) {
        var randIndex = Helpers.getRandomInt(listLamarHypeGifs.length);
        msg.channel.send(`${listLamarHypeGifs[randIndex]}`);
    }
};

const listLamarHypeGifs = [
    "https://tenor.com/view/lamar-jackson-run-running-touchdown-baltimore-ravens-gif-15566788",
    "https://tenor.com/view/lamar-jackson-action-jackson-ankle-break-football-gif-15705957",
    "https://tenor.com/view/broken-foot-football-lamar-jackson-ravens-gif-15752910",
    "https://tenor.com/view/lamar-jackson-baltimore-ravens-eating-banana-nfl-gif-15579216",
    "https://tenor.com/view/lamar-ravens-jackson-baltimore-gif-13048076",
    "https://media.giphy.com/media/WtDXifSTYuGb9NbXlI/giphy.gif"

];