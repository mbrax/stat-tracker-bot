const { SlashCommandBuilder } = require('@discordjs/builders');
const yahooApi = require('../../clients/yahooFinance.js');
const Canvas = require('canvas');
const { ChartJSNodeCanvas } = require('chartjs-node-canvas');
require('chartjs-adapter-luxon');
const {Chart, BarController, } = require('chart.js');
require('../../chartjs-chart-financial.js');
const fs = require('fs');
const {luxon, DateTime, Settings} = require('luxon');


var chartPlugin = {
    beforeDraw: function (chart, easing) {
        if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
            var helpers = Chart.helpers;
            var ctx = chart.ctx;
            var chartArea = chart.chartArea;

            ctx.save();
            ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
            ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
            ctx.restore();
        }
    }
};

module.exports = {
    validChannels: ['789195961367920641'],
    data: new SlashCommandBuilder()
        .setName('chart')
        .setDescription('show a chart for the provided symbol. Optional intervals and date ranges.')
        .addStringOption(option =>
            option.setName('symbol')
                .setDescription('symbol/ticker to search')
                .setRequired(true))
        .addStringOption(option => 
            option.setName('interval')
                .setDescription('Interval for chart to be built.')
                .setRequired(false)
                .addChoices(
                    { name: '1 minute', value:'1m'},
                    { name: '2 minutes', value:'2m'},
                    { name: '5 minutes', value:'5m'},
                    { name: '15 minutes', value:'15m'},
                    { name: '1 hour', value:'60m'},
                    { name: '1 day', value:'1d'},
                ))
        .addStringOption(option => 
            option.setName('range')
                .setDescription('Date range for which to pull data.')
                .setRequired(false)
                .addChoices(
                    { name: '1 day', value:'1d'},
                    { name: '5 days', value:'5d'},
                    { name: '1 month', value:'1mo'},
                    { name: '3 months', value:'3mo'},
                    { name: '6 months', value:'6mo'},
                    { name: '1 year', value:'1y'},
                    { name: '2 years', value:'2y'},
                    { name: '5 years', value:'5y'},
                    { name: '10 years', value:'10y'},
                    { name: 'year to date', value:'ytd'},
                    { name: 'max', value:'max'},
                )),
    async execute(interaction) {
        console.log('here');
        const symbol = interaction.options.getString('symbol');
        const interval = interaction.options.getString('interval') ?? '5m';
        const range = interaction.options.getString('range') ?? '1d';
        yahooApi.getChartForTicker(symbol, interval, range, function(chartData) {
            //console.log(barData);
            if(chartData)
            {  
                var quotes = chartData[0].indicators.quote[0];
                var index;
                var newQuotes = [];
                var times = chartData[0].timestamp;
                if(times)
                    {
                        for(index = 0; index < times.length; index++)
                        {
                            if(quotes.open[index] && quotes.close[index] && quotes.low[index] && quotes.close[index])
                            {
                                    newQuotes.push({
                                        t: times[index],
                                        o: quotes.open[index],
                                        h: quotes.high[index],
                                        l: quotes.low[index],
                                        c: quotes.close[index]
                                    })
                            }
                        }
                    
                    
                        newQuotes.forEach(function(e) {
                            e.t *= 1000;
                        });

                        const width = 1920; //px
                        const height = 1080; //px
                    
                        // Disable animation (otherwise charts will throw exceptions)
                        const canvas = Canvas.createCanvas(width, height);
                        const context = canvas.getContext('2d');
                        var chart = new Chart(context, {
                            type: 'candlestick',
                                    data: {
                                        datasets: [{
                                            label: symbol,
                                            data: newQuotes,
                                            //uncomment this for neon
                                            // color: {
                                            //     up: '#01ff01',
                                            //     down: '#fe0000',
                                            //     unchanged: '#999',
                                            // }
                                        }],
                                    
                                    },
                                    options: {
                                        legend: {
                                            labels: {
                                               boxWidth: 0
                                            }
                                        },
                                        animation: {
                                            duration: 0
                                        },
                                        chartArea: {
                                            backgroundColor: 'rgba(0, 0, 0, 1)'//black
                                        },
                                        responsive: false,
                                        scales: {
                                            x: {
                                                ticks:{
                                                    color: '#FFFFFF',//white
                                                    font: {
                                                        size: 20
                                                    }
                                                },
                                                time: {
                                                    displayFormats: {
                                                        minute: 'HH:mm'
                                                    }
                                                },
                                                gridLines: {
                                                    display: false
                                                }
                                            },
                                            y: {
                                                ticks:{
                                                    color: '#FFFFFF',//white
                                                    font: {
                                                        size: 20
                                                    }
                                                },
                                                gridLines: {
                                                    color: '#666'//greyish.taken from default chart value
                                                }
                                            }
                                        },
                                        plugins:{
                                            legend:{
                                                labels:{
                                                    boxWidth: 0,
                                                    color:'#FFFFFF',
                                                    font:{
                                                        size: 75,
                                                    }
                                                },
                                            }
                                        }
                                    },
                                    plugins: [chartPlugin]
                        });

                        var newCanvas = chart.canvas;

                        newCanvas.toDataURL('image/png', (error, png) => {
                            chart.destroy();
                            if(error)
                            {
                                console.log(error);
                                return;
                            }
                            var base64Data = png.replace(/^data:image\/png;base64,/, "");
                            fs.writeFile('chart.png', base64Data, 'base64', function(err) {
                                interaction.reply({
                                    files: [{
                                    attachment: 'chart.png',
                                    name: 'chart.png'
                                    }]
                                });
                            });
                        });
                    }
            } else {
                interaction.reply(`Could not find data for ${symbol}`);
            }
        });
    }
}
