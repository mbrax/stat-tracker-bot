const { SlashCommandBuilder } = require('@discordjs/builders');
const yahooApi = require('../../clients/yahooFinance.js');


module.exports = {
    validChannels: ['789195961367920641'],
    data: new SlashCommandBuilder()
        .setName('trending')
        .setDescription('get the top 10 trending tickers according to yahoo'),
    async execute(interaction) {
        yahooApi.getTrending(function(trendingData) {
            if(trendingData)
            {
                var results = trendingData[0].quotes;
                var trendingList = "\n";
                var i;
                for(i = 0; i < 10; i++)
                {
                    trendingList += `${i+1}. ${results[i].symbol} \n`;
                }
                interaction.reply(trendingList);
            }
        });
    }
}