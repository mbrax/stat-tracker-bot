const fs = require('fs');
const FeatureRequest = require('../dataModels/FeatureRequest.js');
const Helpers = require('../helpers.js');

module.exports = {
    name: 'request',
    description: "Request something to be added to the bot.",
    directMessage: true,
    args: true,
    usage: ['[feature request]'],
	execute(msg, args, client) {
        createRequest(msg, args, client);
    }
};



async function createRequest(msg, args, client)
{
    //concat the args to make the request
    const request = args.join(' ');
    var toAdd = new FeatureRequest({user: msg.author.id, request: request});
    toAdd.save(async function (err, result){
        if(!err)
        {
            msg.reply(`Your request has been added! Use this value to reference it in other commands: ${result._id}`);
            msg.react('🤘');
            //send message to me with the info
            var guild = await client.guilds.fetch('693568339988971622');
            var me = await guild.members.fetch('155390181065621504');
            me.send(`${msg.author.username} has submitted a new request:\n ${result._id}: ${request}`)

        }else{
            msg.reply(`There was an error saving the request :( `);
        }
    });
}