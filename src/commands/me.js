const mongoose = require('mongoose')
const SecretUsage = require('../dataModels/SecretUsage.js');

module.exports = {
    name: 'me',
    description: "Show how many secrets you've initiated.",
    validChannels:['701133179427487754'],
	execute(msg) {
        //go to db and pull back all records where user is the id of the msg author.
        //display distinct count

        listSecrets(msg.author.id).then((result) => {
            msg.reply(result);
        });


    }
};


async function listSecrets(user)
{
    const secrets =  await SecretUsage
    .find({user:user})
    .select('secret -_id');

    const filtered = secrets.reduce((a, o) => (a.push(o.secret), a), []) 

    let unique = [...new Set(filtered)]

    return unique.length;
}