const fs = require('fs');
const FeatureRequest = require('../dataModels/FeatureRequest.js');
const Helpers = require('../helpers.js');

module.exports = {
    name: 'listrequests',
    aliases:['requests'],
    description: "View your requests.",
    directMessage: true,
	execute(msg) {

        var requestList = "\n";
        FeatureRequest.find({user:msg.author.id}, function (err, result) {
            if(result != null)
            {
              result.forEach((r, i) => {
                requestList += `${r._id}: ${r.request}\n`
              })
              msg.reply(requestList);
            }
        });
    }
};