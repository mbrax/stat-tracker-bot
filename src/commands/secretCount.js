const fs = require('fs');

module.exports = {
    name: 'secretcount',
    aliases: ['secrets'],
    description: "How many secrets there are to find. Some secrets have multiple ways to activate them.",
    validChannels:['701133179427487754'],
	execute(msg) {
        var secretsList = [];
        const secretFiles = fs.readdirSync('./secrets').filter(file => file.endsWith('.js'));
        var aliasesList = [];
        for (const file of secretFiles) {
            const secret = require(`../secrets/${file}`);
            secretsList.push(secret);
            if(secret.aliases)
            {
                secret.aliases.forEach(a => {
                    aliasesList.push(a);
                });
            }
        }

        msg.reply(`${secretFiles.length} unique secrets with ${aliasesList.length} additional aliases. Total: ${secretFiles.length + aliasesList.length}`);
    }
};