module.exports = {
    name: 'happybirthday',
    description: "Send someone a special gift.",
    usage: '[mention a user]',
    args: true,
    cooldown: 30,
    validChannels:['701133179427487754'],
	execute(msg) {
        if (!msg.mentions.users.size) {
            return;
        }
        
        const taggedUser = msg.mentions.users.first();

        msg.channel.send(`${taggedUser}, happy birthday! You have a special gift from ${msg.member}`,
        {
            files: [{
            attachment: 'images/luke_freshman_card.png',
            name: 'luke_freshman_card.png'
            }]
        });
    }
};