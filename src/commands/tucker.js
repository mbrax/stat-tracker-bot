const Helpers = require('../helpers.js');

module.exports = {
    name: 'tucker',
    aliases: ['jt'],
    description: "",
	execute(msg) {
        var randIndex = Helpers.getRandomInt(listTuckerGifs.length);
        msg.channel.send(`${listTuckerGifs[randIndex]}`);
    }
};

const listTuckerGifs = [
    "https://tenor.com/view/ravens-baltimore-ravens-touchdown-gif-3562287",
    "https://tenor.com/view/justin-ravens-tucker-dab-football-gif-7385687",
    "https://tenor.com/view/justin-tucker-baltimore-ravens-gif-12746796",
    "https://tenor.com/view/justin-justin-tucker-lit-swag-dance-gif-15922434",
    "https://tenor.com/view/justin-tucker-baltimore-ravens-am-i-good-or-what-football-nfl-gif-12746785",
    "https://tenor.com/view/baltimore-ravens-dancing-justin-tucker-hotline-bling-dance-gif-11453677",
    "https://tenor.com/view/baltimore-ravens-justin-tucker-field-goal-kicker-ravens-gif-15661165"
];