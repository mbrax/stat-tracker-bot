const fs = require('fs');
const FeatureRequest = require('../dataModels/FeatureRequest.js');
const Helpers = require('../helpers.js');

module.exports = {
    name: 'cancelrequest',
    description: "Request something to be added to the bot.",
    directMessage: true,
    args: true,
    usage: ['[id to cancel]'],
	execute(msg, args) {

        var idToCancel = args[0];

        FeatureRequest.findByIdAndDelete({_id: idToCancel}, (err, result) => {
            if(!err)
            {
                if(!result)
                {
                    msg.reply(`A request was not found with that id.`);
                }
                else
                {
                    msg.reply(`Your request has been canceled!`);
                    msg.react('🤘');
                }
            }else{
                msg.reply(`There was an error canceling the request :( `);
            }
        });
    }
};