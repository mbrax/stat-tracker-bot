module.exports = {
    name: 'rat',
    aliases: ['rats'],
    description: "",
    cooldown: 300,//5 minutes
	execute(msg) {
        const voiceChannel = msg.member.voice.channel;
        if(!voiceChannel)
        {
            msg.reply("You need to be in a voice channel to play music.");
            return;
        }
        
        const permissions = voiceChannel.permissionsFor(msg.client.user);
        if (!permissions.has("CONNECT") || !permissions.has("SPEAK")) {
            msg.reply("I need the permissions to join and speak in your voice channel!");
            return;
        }

        voiceChannel.join()
            .then(connection => {
                const dispatcher = connection.play('audio/rats.mp3');
                
                 dispatcher.on("speaking", (speaking) => {
                    if (!speaking) {
                        voiceChannel.leave();
                    }
                });
            })
            .catch(console.error);
    }
};