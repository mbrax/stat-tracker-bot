const Helpers = require('../helpers.js');

module.exports = {
    name: 'touchdown',
    aliases: ['td'],
    description: "",
	execute(msg) {
        var randIndex = Helpers.getRandomInt(listTouchdownGifs.length);
        msg.channel.send(`TOUCHDOWN!\n${listTouchdownGifs[randIndex]}`);
    }
};

const listTouchdownGifs = [
    "https://tenor.com/view/baltimore-ravens-ravens-ravensflock-champion-gif-13132358",
    "https://tenor.com/view/mark-andrews-mark-andrews-ravens-baltimoreravens-gif-19554556",
    "https://tenor.com/view/baltimore-ravens-lamar-jackson-ravens-ravens-touchdown-ravens-win-gif-19983383"
];