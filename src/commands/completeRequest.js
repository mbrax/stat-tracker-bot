const fs = require('fs');
const FeatureRequest = require('../dataModels/FeatureRequest.js');
const Helpers = require('../helpers.js');

module.exports = {
    name: 'completerequest',
    description: "Complete a request",
    directMessage: true,
    args: true,
    usage: ['[id to cancel]'],
	execute(msg, args, client) {

        var idToComplete = args[0];
        completeRequest(msg, idToComplete, client);
    }
};

async function completeRequest(msg, requestId, client)
{
    FeatureRequest.findByIdAndDelete({_id: requestId}, async (err, result) => {
        if(!err)
        {
            if(!result)
            {
                msg.reply(`This request was already completed or does not exist.`);
            }
            else{
                var guild = await client.guilds.fetch('693568339988971622');
                var member = await guild.members.fetch(result.user);
                member.user.send(`Request ${requestId} has been completed!`);
                msg.reply(`Request completed.`);
            }
        }else{
            msg.reply(`There was an error completeing the request :( `);
        }
    });
}
