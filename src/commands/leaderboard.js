const fs = require('fs');
const SecretUsage = require('../dataModels/SecretUsage');
const Helpers = require('../helpers.js');

module.exports = {
    name: 'leaderboard',
    description: "Leaderboard of users and how many unique secrets they've used.",
    validChannels:['701133179427487754'],
	execute(msg) {
       
        getLeaderboard(msg).then((result) => {
            msg.reply(result);
        });
    }
};



async function getLeaderboard(msg)
{
    const aggQuery = [
        {
          '$match': {
            'user': {
              '$ne': '155390181065621504'
            }
          }
        }, {
          '$group': {
            '_id': '$user', 
            'secrets': {
              '$addToSet': '$secret'
            }
          }
        }, {
          '$project': {
            'user': '$user', 
            'secretCount': {
              '$size': '$secrets'
            }
          }
        }, {
          '$sort': {
            'secretCount': -1
          }
        }
      ];

    const agg = await SecretUsage.aggregate(aggQuery);

    var leaderboard = "\n";

    await Helpers.asyncForEach(agg, async (user, i) => {
        var member = await msg.guild.members.fetch(user._id);
            console.log(member.user.username);
            leaderboard += `${i+1}. ${member.user.username} with ${user.secretCount} \n`;
    });
   
    return leaderboard;
}