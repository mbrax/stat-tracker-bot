const unirest = require('unirest');

exports.getChartForTicker = function(ticker, interval, range, callback) {
    var req = unirest("GET", "https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-chart");
    req.query({
        "interval": interval,
        "symbol": ticker,
        "range": range,
        "region": "US"
    });
    req.headers({
        "x-rapidapi-key": "83998e05d1msh2041b78f0603577p15e77djsn9c5b20069731",
        "x-rapidapi-host": "apidojo-yahoo-finance-v1.p.rapidapi.com",
        "useQueryString": true
    });

    req.end(function (res) {
        if (res.error)
        { 
            console.log(res.error);
            return;
        }
        callback(res.body.chart.result);
    });
};


exports.getTrending = function(callback) {
    var req = unirest("GET", "https://apidojo-yahoo-finance-v1.p.rapidapi.com/market/get-trending-tickers");
    req.query({
        "region": "US"
    });
    req.headers({
        "x-rapidapi-key": "83998e05d1msh2041b78f0603577p15e77djsn9c5b20069731",
        "x-rapidapi-host": "apidojo-yahoo-finance-v1.p.rapidapi.com",
        "useQueryString": true
    });

    req.end(function (res) {
        if (res.error)
        { 
            console.log(res.error);
            return;
        }
        callback(res.body.finance.result);
    });
}