const { Client, Intents, Collection } = require('discord.js');
const database = require('./database')
const mongoose = require('mongoose')
const { Console } = require('console');
const SecretUsage = require('./dataModels/SecretUsage');
const fs = require('fs');
const Helpers = require('./helpers.js');
const unirest = require("unirest");
const { aggregate } = require('./dataModels/SecretUsage');

const {luxon, DateTime, Settings} = require('luxon');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const { token } = require('./config.json');

Settings.defaultZoneName = 'America/New_York';

//connect to db
mongoose.connect(database.url)
.then(() => {
    console.log('MongoDB connected...');
})
.catch(err => {
    console.log(err); 
    process.exit(1);
})

const prefix = "!";

//connect to discord
 const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_VOICE_STATES] });

 client.commands = new Collection();
 client.stockCommands = new Collection();
 client.secrets = new Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
const stockCommandFiles = fs.readdirSync('./commands/stocks').filter(file => file.endsWith('.js'));
const secretFiles = fs.readdirSync('./secrets').filter(file => file.endsWith('.js'));


for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name.toLowerCase(), command);
}

const stockCommands = [];
for (const file of stockCommandFiles) {
	const command = require(`./commands/stocks/${file}`);
	client.stockCommands.set(command.data.name.toLowerCase(), command);
    stockCommands.push(command.data.toJSON());

}

for (const file of secretFiles) {
	const secret = require(`./secrets/${file}`);
	client.secrets.set(secret.name.toLowerCase(), secret);
}

const clientId = '748634322109726924';
const guildId = '693568339988971622';

// registering slash commands for stock commands
const rest = new REST({ version: '9' }).setToken(token);
(async () => {
	try {
		console.log('Started refreshing application (/) commands.');

		await rest.put(
			Routes.applicationGuildCommands(clientId, guildId),
			{ body: stockCommands },
		);

		console.log('Successfully reloaded application (/) commands.');
	} catch (error) {
		console.error(error);
	}
})();


const cooldowns = new Collection();



client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
 });


 client.on('interactionCreate', async interaction => {
     if(!interaction.isCommand()) return;

     const command = client.stockCommands.get(interaction.commandName);

     if(!command) return;

     if(command.validChannels && !command.validChannels.includes(interaction.channel.id) && interaction.channel.id != '757289818828046477') {//bot testing channel is 757289818828046477
        await interaction.reply({ content: 'This command cannot be used in this channel.', ephemeral: true });
        return;
     }

     try{
         await command.execute(interaction);
     } catch (error) {
        console.error(error);
        await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
     }
 })

 //code to create a text channel that keeps roles updated with who is in the equivalent voice channel
 client.on("voiceStateUpdate", (oldState, newState) => {
    //if old state was a channel, remove it
    var person = oldState.member;
    var oldChannel = oldState.channel;
    if (oldChannel != null)
    {
      //  console.log(`Old channel: ${oldChannel.name.toLowerCase()}`);
        var role = person.guild.roles.cache.find(r => r.name === oldChannel.name.toLowerCase());
        if(role != null)
        {
           // console.log(`Old role ${role.name}`);
            person.roles.remove(role);
            console.log(`Role ${role.name} removed from user ${person.displayName}`);
        }
    }
    
    //check if new state has a channel
    var newChannel = newState.channel;
    if(newChannel != null)
    {
      //  console.log(`New channel: ${newChannel.name.toLowerCase()}`);
        var role = person.guild.roles.cache.find(r => r.name === newChannel.name.toLowerCase());
        if(role != null)
        {
          //  console.log(`New role ${role.name}`);
            person.roles.add(role);
            console.log(`Role ${role.name} added to user ${person.displayName}`);
        }
    }
 })

client.on('messageCreate', msg => {
    if(msg.author.bot) return;
    msg.content = msg.content.toLowerCase();
    //2 types of messages we are looking for
    //commands -> start with the prefix
    //secrets -> are just hidden within the message and not a prefixed command
  //  if(msg.channel == '757289818828046477')
 // {
    if(msg.content.startsWith(prefix))
    {
        const args = msg.content.slice(prefix.length).trim().split(/ +/);
        const commandName = args.shift();

        const command = client.commands.get(commandName)
        || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
        
        if (!command) return;

        if(command.validChannels && !command.validChannels.includes(msg.channel.id) && msg.channel.id != '757289818828046477')//bot testing channel is 757289818828046477
            return;

        if(command.directMessage && msg.channel.type != 'dm')
            return msg.reply('That command can only be executed in a direct message!');

        if (command.args && !args.length) {
            let reply = `You didn't provide any arguments, ${msg.author}!`;
    
            if (command.usage) {
                reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
            }

            return msg.channel.send(reply);
        }

        if (!cooldowns.has(command.name)) {
            cooldowns.set(command.name, new Discord.Collection());
        }

        const now = Date.now();
	    const timestamps = cooldowns.get(command.name);
	    const cooldownAmount = (command.cooldown || 3) * 1000;

	    if (timestamps.has(msg.author.id)) {
		    const expirationTime = timestamps.get(msg.author.id) + cooldownAmount;

            if (now < expirationTime) {
                const timeLeft = (expirationTime - now) / 1000;
                return msg.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`);
            }
	    }

        timestamps.set(msg.author.id, now);
        setTimeout(() => timestamps.delete(msg.author.id), cooldownAmount);

        try {
            command.execute(msg, args, client);
        } catch (error) {
            console.error(error);
            msg.reply('there was an error trying to execute that command!');
        }
    }
    else if(msg.content.startsWith('$') || msg.content.startsWith('£') && (msg.channel.id === '789195961367920641' || msg.channel.id === '757289818828046477'))
    {
        msg.reply('https://giphy.com/gifs/mcH0upG1TeEak')
        
        // const args = msg.content.slice(prefix.length).trim().split(/ +/);
        // const commandName = args.shift();

        // const command = client.stockCommands.get(commandName)
        // || client.stockCommands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
        
        // if (!command && (msg.channel.id === '757289818828046477' || msg.channel.id === '789195961367920641'))
        // {
        //     if(!commandName)
        //     {
        //         console.log("invalid argument for stock commands. invalid command name.")
        //         return;
        //     }
        //     const interval = args[0] ? args[0] : '5m';
        //     const range = args[1] ? args[1] : '1d';

        //     const validIntervals = ['1m','2m','5m','15m','60m','1d'];
        //     const validRanges = ['1d','5d','1mo','3mo','6mo','1y','2y','5y','10y','ytd','max'];

        //     if(!validIntervals.includes(interval))
        //     {   
        //         msg.reply(`That is not a valid interval. Use a value in this set: ${validIntervals}`);
        //         return;
        //     }

        //     if(!validRanges.includes(range))
        //     {   
        //         msg.reply(`That is not a valid range. Use a valid in this set: ${validRanges}`);
        //         return;
        //     }

          

            
    //     }
    //     else
    //     {
    //         if (!command) return;

    //         if(command.validChannels && !command.validChannels.includes(msg.channel.id) && msg.channel.id != '757289818828046477')//bot testing channel is 757289818828046477
    //         return;

    //         try {
    //             command.execute(msg, args, client);
    //         } catch (error) {
    //             console.error(error);
    //             msg.reply('there was an error trying to execute that command!');
    //         }
    //     }
    // }
     } else
    {
        //secrets get triggered if the name or aliases has the secret AND ignores does not have it
        const filteredSecrets = client.secrets.filter(secr => (msg.content.search(new RegExp("\\b" + secr.name + "\\b")) >= 0
             || (secr.aliases && secr.aliases.some(a => msg.content.search(new RegExp("\\b" + a + "\\b")) >= 0)))
             && (!secr.ignores || secr.ignores.every(i => msg.content.search(new RegExp("\\b" + i + "\\b")) === -1))
        );
        const secrets = Array.from(filteredSecrets, ([_, secret]) => secret);

        if(Helpers.checkForBritish(msg.content) && msg.author.id === '689600612949557375')//Tim
        {
            msg.react('🇬🇧');
        }

        if(secrets === undefined || secrets.length === 0) {
            console.log('nothing')   ;
            return;
        }
        

        for(const secret of secrets)
        {
            if(secret.validChannels && !secret.validChannels.includes(msg.channel.id) && msg.channel.id != '757289818828046477')//bot testing channel is 757289818828046477
                return;
            
           // console.log(secret);
            if(!cooldowns.has(secret.name))
            {
                cooldowns.set(secret.name, new Collection());
            }
    
            const now = Date.now();
            const timestamps = cooldowns.get(secret.name);
            const cooldownAmount = (secret.cooldown || 3) * 1000;
    
            if(timestamps.has(msg.author.id))
            {
                const expirationTime = timestamps.get(msg.author.id) + cooldownAmount;
    
                if(now < expirationTime)
                {
                    const timeLeft = (expirationTime - now) / 1000;
                    return msg.reply(`please wait ${timeLeft.toFixed(1)} more seconds before reusing that secret.`);
                }
            }
    
            timestamps.set(msg.author.id, now);
            setTimeout(() => timestamps.delete(msg.author.id), cooldownAmount);

            try{
                secret.execute(msg);
            }
            catch(error)
            {
                console.error(error);
		        msg.reply('there was an error trying to execute that secret!');
            }

            try
            {
                //store the usage in mongodb
                var usage = new SecretUsage({user:msg.author.id, secret:secret.name});
                usage.save();
            }
            catch(error)
            {
                console.log(error);
            }
        }
    }
//}
 });

client.login('NzQ4NjM0MzIyMTA5NzI2OTI0.X0gSCw._zMmN4rquO_X4Mqctlac8beCbGE');

