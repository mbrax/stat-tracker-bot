const translator = require('american-british-english-translator');

exports.getRandomInt = function(max) {
    var r = Math.floor(Math.random() * Math.floor(max));
    console.log(`Random generator: ${max} : ${r}`)
    return r
  }

exports.asyncForEach = async function(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};


exports.checkForBritish = function(msg) {
  var options = {
    british: true
  };
    var result = translator.translate(msg, options);
    var values = Object.values(result);
    var diffs = values[0];
    if(diffs && diffs.some(diff => Object.values(diff).some(x => x.issue === 'British English Spelling')))
    {
      return true;
    }
    return false;
}