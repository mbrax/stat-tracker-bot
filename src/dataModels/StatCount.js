//External dependencies
const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    statName: String,
    count: Number
})

module.exports = mongoose.model('StatCount', schema, "statCounts")