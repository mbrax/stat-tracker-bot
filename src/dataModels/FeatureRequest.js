//External dependencies
const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    user: String,
    request: String
})

module.exports = mongoose.model('FeatureRequest', schema, "featureRequests")