//External dependencies
const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    user: String,
    secret: String
})

module.exports = mongoose.model('SecretUsage', schema, "secretUsage")